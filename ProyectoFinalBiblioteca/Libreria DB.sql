﻿create database Libreria3
execute sp_addtype member_no ,'INT' ,'NOT NULL'
execute sp_addtype title  ,'varchar(20)' ,'NOT NULL'
execute sp_addtype title_no  ,'INT' ,'NOT NULL'
EXEC  sp_addtype isbn, 'smallint', 'NOT NULL'
EXEC  sp_addtype zipcode, 'char(10)'
go
--------------------Procedimientos almacenados-------------------------
use Libreria3
execute Alta_Miembro_Adulto
@member_no = 11,
@apellido = 'Valdez',
@nombre = 'Josue',
@inicial = 'V',
@calle = 'Rio Elota',
@codigoP = '81240',
@ciudad = 'Culiacan',
@estado = 'Sinaloa',
@celular = '6688257691',
@expdate = '01-01-2018';

use Libreria3
execute Alta_Miembro_Joven
@member_no = 16,
@nom = 'Guillermo',
@ape = 'Juarez',
@ini = 'J',
@adul_member_no = 15,
@fecha_nac = '12-17-2005'

use Libreria3
execute Baja_Miembro_Joven
@member_no = 16

use Libreria3
execute Baja_Miembro_Adulto
@member_no = 15

use Libreria3
execute Alta_reservacion
@isbn = 101,
@copy_no = 1,
@title_no = 10001,
@member_no = 11,
@due_date = '12-05-2016'

use Libreria3
execute Baja_reservacion
@isbn = 102,
@copy_no = 1,
@title_no = 10001,
@member_no = 11,
@remarks = 'Muy mal, se tardó demasiadoy esta muy maltratado'

select * from loan
go

select* from COPY
go

select * from loanhist
go

 
update adult set  expr_date = '12-02-2011' where member_no = 12
update loan set due_date = '08-30-2016' where isbn = 102 and copy_no = 1
update loan set out_date = '08-29-2016' where isbn = 102 and copy_no = 1
update copy set on_loan = 'no' where isbn = 100 and copy_no = 2
-----------------------------------------------------------
create table member
(
member_no member_no primary key,
lastname char(20) not null,
middleinitial char(1)
)
go

create table adult
(
member_no member_no primary key references member(member_no),
street char(20) not null,
zipcode zipcode check (zipcode like '[0-9][0-9][0-9][0-9][0-9]'),
city char(20),
state char(20),
phone_no char(20)
)
go

alter table adult drop constraint adult_phone_ck
go

ALTER TABLE ADULT 
ADD CONSTRAINT adult_phone_ck CHECK 
(phone_no IS NULL OR 
 phone_no LIKE '[0-9][0-9][0-9][0-9][0-9][0-9][0-9]' OR
 phone_no LIKE
'[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]')
go

alter table adult --Agregar fecha de expiracion de todos los afultos agregados y despues ponerle que no acepte nulos
ADD expr_date datetime null
go

alter table member
add name char(50) null
go

create table juvenile
(member_no member_no constraint juvenile_pk primary key,
 adult_member_no member_no  constraint juvenile_fk
 references adult(member_no),
 birthdate datetime not null,
 constraint juvenile_fk2 foreign key(member_no) references member(member_no)
)
go

ALTER TABLE JUVENILE 
ADD CONSTRAINT JUVENILE_birthdate_CK 
CHECK (birthdate < getdate())
go
------------------Inserción de datos------------------------------------------
insert into member values (1, 'Mendez', 'M');
insert into member values (2, 'Mendez', 'M');
insert into member values (3, 'Marquez', 'M');
insert into member values (4, 'Ruiz', 'R');
insert into member values (5, 'Lopez', 'L');
insert into member values (6, 'Lopez', 'L');
go

insert into adult values (1,'Rio Sinaloa',81240,'Culiacan','Sinaloa',6677145890);
insert into adult values (3,'Rio Culiacan',81240,'Culiacan','Sinaloa',6674414112);
insert into adult values (4,'Serdan',81240,'Los Mochis','Sinaloa',6677878741);
insert into adult values (5,'Obregon',81240,'Culiacan','Sinaloa',6673176397);
go

update adult set expr_date = '11-29-2018' where member_no in(1,3,4,5,7,8,9)

update member set name = 'Mario' where member_no in(1,3,4,5,7,8,9,10)
update member set name = 'Juan Jose' where member_no in(2,6)

insert into juvenile values(2,1,'10-10-2000');
insert into juvenile values(6,5,'12-12-2002');
go

SELECT CONCAT(m.name, ' ' ,m.lastname) AS Nombre , m.member_no FROM member m

SELECT M.member_no, m.middleinitial, m.lastname,m.name,a.street,a.zipcode,a.city,a.state,a.phone_no,CONVERT(date,a.expr_date) AS Expiracion
FROM member m
INNER JOIN adult a ON a.member_no = m.member_no
GROUP BY  M.member_no, m.middleinitial, m.lastname,m.name,a.street,a.zipcode,a.city,a.state,a.phone_no,CONVERT(date,a.expr_date)
GO

SELECT  m.member_no,m.lastname,m.name,m.middleinitial,j.adult_member_no,CONVERT(date,j.birthdate) AS Nacimiento
FROM member m
INNER JOIN juvenile j ON j.member_no = m.member_no
GROUP BY m.member_no,m.lastname,m.name,m.middleinitial,j.adult_member_no,CONVERT(date,j.birthdate)
GO

SELECT * FROM adult a
JOIN juvenile j ON j.adult_member_no = a.member_no
go

SELECT M.member_no, m.middleinitial, m.lastname,m.name,a.street,a.zipcode,a.city,a.state,a.phone_no,CONVERT(date,a.expr_date) AS Expiracion
FROM member m
INNER JOIN adult a ON a.member_no = m.member_no
WHERE a.member_no = 1
GROUP BY  M.member_no, m.middleinitial, m.lastname,m.name,a.street,a.zipcode,a.city,a.state,a.phone_no,CONVERT(date,a.expr_date)
GO

SELECT CONCAT(m.name, ' ' ,m.lastname) FROM member m WHERE m.member_no = 1
-------------------------------------------------------------------------------------------------------
create table title
(
title_no title_no constraint title_pk primary key,
title title not null,
author char(20) not null,
synopsis varchar(100) 
)
go

CREATE TABLE item 
(
 isbn isbn,
 title_no title_no references title(title_no),
 idioma VARCHAR(20) NOT NULL,
 cover VARCHAR(20) NULL,
 loanable char(2),
 constraint item_pk primary key (isbn)
)
go

alter table item add constraint item_loanable_ck 
check (loanable ='si' or loanable='no')
go

alter table item add constraint item_loanable_df 
default 'si' for loanable
go

CREATE TABLE COPY 
(
isbn isbn,
copy_no int not null,
title_no title_no constraint copy_title_fk references title(title_no),
on_loan char (2) constraint copy_df default 'no'
constraint copy_pk primary key (isbn,copy_no)
)
go

alter table copy 
add constraint copy_isbn_fk 
foreign key(isbn)  references item(isbn)
go
---------------------------------Captura de datos------------------------------------------------------------
insert into title values(10001,'El Alquimista', 'Paolo Cohelo','El niño que vivio');
insert into title values(10002,'Harry Potter 1', 'J. K. Rowling','El mago mas peculiar');
insert into title values(10003,'Quiubole', 'Jordy Rosado','No puede estar al alcance de las mujeres');
insert into title values(10004,'Los juegos del hambr', 'Suzane Collins','Que los juegos del hambre empiecen');
go

insert into item values(100,10001,'Español','Delgada','si')
insert into item values(101,10001,'Ingles','Delgada','si')
insert into item(isbn,title_no,idioma,cover) values(102,10001,'Ingles','Dura')
insert into item(isbn,title_no,idioma,cover) values(103,10002,'Ingles','Dura')
insert into item(isbn,title_no,idioma,cover) values(104,10003,'Español','Dura')
go

insert into copy(isbn,copy_no,title_no) values(100,1,10001)
insert into copy(isbn,copy_no,title_no) values(100,2,10001)
insert into copy(isbn,copy_no,title_no) values(101,1,10001)
insert into copy(isbn,copy_no,title_no) values(102,1,10001)
insert into copy(isbn,copy_no,title_no) values(103,1,10002)
insert into copy(isbn,copy_no,title_no) values(103,2,10002)
go


select t.title from title t where t.title_no = 10001
--Copias disponibles por libro--
select t.title,t.title_no,c.isbn,c.copy_no from title t join item i on i.title_no = t.title_no join copy c on c.isbn = i.isbn where on_loan = 'no'
--------------------------------------------------------------------------------------------------------------------
CREATE TABLE loan 
(
isbn isbn, 
copy_no int not null,
title_no int not null constraint loan_title_no_fk 
references title(title_no),
member_no int not null constraint loan_member_fk 
references member(member_no),
out_date datetime not null,
due_date datetime not null,
constraint loan_pk primary key (isbn, copy_no,out_date),
constraint loan_isb_copy_fk foreign key (isbn, copy_no) references copy (isbn, copy_no)
)

alter table loan 
add constraint loan_due_out_ck check(due_date>out_date)

create table reservation
(
isbn isbn constraint reservation_fk references item (isbn),
member_no member_no constraint reservation_fk2 references member(member_no),
log_date datetime,
remarks varchar(100),
constraint reservation_pk primary key (isbn)
)

alter table reservation 
drop constraint reservation_pk 

alter table reservation 
add constraint reservation_pk primary key (isbn, member_no)

CREATE TABLE loanhist
(
isbn isbn,
copy_no int not null,
out_date datetime not null,
title_no int not null references title(title_no),
member_no int not null references member(member_no),
due_date datetime not null,
int_date datetime not null,
fine_assessed float not null,
fine_paid float not null,
fine_waived float not null,
remarks varchar(100)
constraint loanhist_PK primary key (isbn,copy_no)
)

alter table loanhist
DROP constraint loanhist_PK

alter table loanhist
ADD constraint loanhist_PK primary key (isbn,copy_no,out_date)
