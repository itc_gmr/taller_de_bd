----------Procedimiento almacenado de miembro adulto------------------------------
Alter PROCEDURE Alta_Miembro_Adulto
@member_no int,
@apellido char(20),
@nombre char(50),
@inicial char(1),
@calle char(20),
@codigoP char(5),
@ciudad char(20),
@estado char(20),
@celular char(10),
@expdate datetime
 AS
 IF EXISTS(SELECT * FROM member WHERE member_no = @member_no)
	BEGIN
		RAISERROR(15600,-1,-1,'Error en la alta del miembro adulto, llave primaria repetida')
		RETURN 1
	END
 ELSE
	BEGIN
		INSERT INTO member VALUES (@member_no, @apellido,@inicial, @nombre)
		INSERT INTO adult VALUES (@member_no,@calle,@codigoP,@ciudad,@estado,@celular,@expdate)
	END;
-----------------------------------------------------------------------------------------------------------