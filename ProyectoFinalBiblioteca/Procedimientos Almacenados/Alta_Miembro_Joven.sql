Alter PROCEDURE Alta_Miembro_Joven
@member_no int,
@nom char(50),
@ape char(20),
@ini char(1),
@adul_member_no int,
@fecha_nac datetime
AS
IF EXISTS(SELECT * FROM member WHERE member_no = @member_no)
	BEGIN
		RAISERROR(15600,-1,-1,'Llave primaria existente')
		RETURN 1
	END
IF NOT EXISTS(SELECT * FROM adult WHERE member_no = @adul_member_no)
	BEGIN
		RAISERROR(15600,-1,-1,'Llave primaria de adulto inexistente')
		RETURN 1
	END
IF (SELECT adult.expr_date FROM adult WHERE adult.member_no = @adul_member_no) < GETDATE()
	BEGIN
		RAISERROR(15600,-1,-1,'Membresia vencida')
		RETURN 1
	END
ELSE
	BEGIN
		INSERT INTO member VALUES (@member_no, @ape, @ini,@nom)
		INSERT INTO juvenile VALUES(@member_no,@adul_member_no,@fecha_nac)
	END
