ALTER TRIGGER Insercion_reservacion
ON loan FOR INSERT
AS
	UPDATE C SET on_loan = 'Si'
	FROM COPY C 
	INNER JOIN inserted I ON C.isbn = I.isbn AND C.copy_no = I.copy_no
	