CREATE PROCEDURE Baja_reservacion
@isbn smallint,
@copy_no int,
@title_no int,
@member_no int,
@remarks char(100)
AS
DECLARE @due_date datetime = (SELECT due_date FROM loan WHERE ISBN = @isbn AND COPY_NO = @copy_no AND TITLE_NO = @title_no AND MEMBER_NO = @member_no)
DECLARE @out_date datetime = (SELECT out_date FROM loan WHERE ISBN = @isbn AND COPY_NO = @copy_no AND TITLE_NO = @title_no AND MEMBER_NO = @member_no)
DECLARE @fine FLOAT
IF NOT EXISTS(SELECT * FROM loan WHERE ISBN = @isbn AND COPY_NO = @copy_no AND TITLE_NO = @title_no AND MEMBER_NO = @member_no)
	BEGIN
		RAISERROR(15600,-1,-1,'No existe dicho pr�stamo')
		RETURN 1
	END
ELSE
	IF GETDATE() <= @due_date
		BEGIN
			DELETE FROM loan WHERE ISBN = @isbn AND COPY_NO = @copy_no AND TITLE_NO = @title_no AND MEMBER_NO = @member_no
			INSERT INTO loanhist VALUES(@isbn,@copy_no,@out_date,@title_no,@member_no,@due_date,GETDATE(),0.0,0.0,0.0,@remarks)
		END
	ELSE
		BEGIN--POR CADA DIA TRANSCURRIDO SON 2.5 PESOS
			SET @fine = (DATEDIFF(DAY,@due_date,GETDATE()) * 2.5)
			DELETE FROM loan WHERE ISBN = @isbn AND COPY_NO = @copy_no AND TITLE_NO = @title_no AND MEMBER_NO = @member_no
			INSERT INTO loanhist VALUES(@isbn,@copy_no,@out_date,@title_no,@member_no,@due_date,GETDATE(),@fine,0.0,0.0,@remarks)
		END