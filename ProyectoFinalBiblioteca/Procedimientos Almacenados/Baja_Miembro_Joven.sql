ALTER PROCEDURE Baja_Miembro_Joven
@member_no int
AS
IF NOT EXISTS(SELECT * FROM juvenile WHERE member_no = @member_no)
	BEGIN
		RAISERROR(15600,-1,-1,'Miembro inexistente')
		RETURN 1
	END
ELSE
	BEGIN
		DELETE FROM juvenile WHERE member_no = @member_no
		DELETE FROM member WHERE member_no = @member_no
	END