ALTER PROCEDURE Alta_reservacion
@isbn smallint,
@copy_no int,
@title_no int,
@member_no int,
--@out_date datetime,
@due_date datetime
AS
IF EXISTS(SELECT adult.member_no FROM adult WHERE adult.member_no = @member_no)--ES ADULTO
	IF (SELECT adult.expr_date FROM adult WHERE adult.member_no = @member_no) < GETDATE()
		BEGIN
			RAISERROR(15600,-1,-1,'Liscencia expirada')
			RETURN 1
		END
	ELSE
		IF (SELECT ON_LOAN FROM COPY WHERE ISBN = @ISBN AND COPY_NO = @COPY_NO) = 'SI'--VALIDAR SI LA COPIA ESTA DISPONIBLE
			BEGIN
				RAISERROR(15600,-1,-1,'Copia de libro prestada')
				RETURN 1
			END
		ELSE
			BEGIN
				INSERT INTO loan VALUES(@isbn,@copy_no,@title_no,@member_no,GETDATE(),@due_date)
			END
ELSE --ES JOVEN
	IF (SELECT a.expr_date FROM adult a JOIN juvenile j ON J.adult_member_no = A.member_no WHERE J.member_no = @member_no) < GETDATE()
		BEGIN
			RAISERROR(15600,-1,-1,'Liscencia expirada')
			RETURN 1
		END
	ELSE
		IF (SELECT ON_LOAN FROM COPY WHERE ISBN = @ISBN AND COPY_NO = @COPY_NO) = 'SI'--VALIDAR SI LA COPIA ESTA DISPONIBLE
			BEGIN
				RAISERROR(15600,-1,-1,'Copia de libro prestada')
				RETURN 1
			END
		ELSE
			BEGIN
				INSERT INTO loan VALUES(@isbn,@copy_no,@title_no,@member_no,GETDATE(),@due_date)
			END
		