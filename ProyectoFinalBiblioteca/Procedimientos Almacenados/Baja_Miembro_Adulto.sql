ALTER PROCEDURE Baja_Miembro_Adulto
@member_no int
AS
IF NOT EXISTS(SELECT * FROM adult WHERE member_no = @member_no)
	BEGIN
		RAISERROR(15600,-1,-1,'Miembro inexistente')
		RETURN 1
	END
IF EXISTS(SELECT * FROM juvenile WHERE adult_member_no = @member_no)
	BEGIN
		RAISERROR(15600,-1,-1,'Hijos registrados')
		RETURN 1
	END
ELSE
	BEGIN
		DELETE FROM adult WHERE member_no = @member_no
		DELETE FROM member WHERE member_no = @member_no
	END