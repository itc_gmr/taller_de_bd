CREATE TRIGGER Retiro_reservacion
ON loan FOR DELETE
AS
	UPDATE C SET on_loan = 'no'
	FROM COPY C 
	INNER JOIN deleted d ON C.isbn = d.isbn AND C.copy_no = d.copy_no