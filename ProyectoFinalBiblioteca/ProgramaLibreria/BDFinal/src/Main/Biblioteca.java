/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Main;
import Datos.Consulta;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import Componentes.MyRandom;
/**
 *
 * @author guill
 */
public class Biblioteca extends javax.swing.JFrame {
    boolean bandConsulta = false;
    boolean bandConsultaJuv = false;
    Object libros[];
    Object miembrosGeneral[];
    Object prestamos[];
    public Biblioteca() {
        initComponents();
        this.panJuvenil.setVisible(false);
        this.panAltaPrestamo.setVisible(false);
        this.panBajaPrestamo.setVisible(false);
        /*DefaultTableModel tm = Consulta.consultaPrestamos();
        this.tablaPrestamos.setModel(tm);
        this.tablaPrestamos.updateUI();
        tm = Consulta.consultaHistorialPrestamos();
        this.tablaHistPrestamos.setModel(tm);
        this.tablaHistPrestamos.updateUI();*/
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        dialogoMiembros = new javax.swing.JDialog();
        jScrollPane2 = new javax.swing.JScrollPane();
        tablaMiembros = new javax.swing.JTable();
        btnConsultaMiembroAdul = new javax.swing.JButton();
        btnEliminarMiembro = new javax.swing.JButton();
        btnRefrescar = new javax.swing.JButton();
        tipoDeMiembro = new javax.swing.ButtonGroup();
        tipoDeOperacion = new javax.swing.ButtonGroup();
        dialogoMiembrosJuv = new javax.swing.JDialog();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaMiembrosJuv = new javax.swing.JTable();
        btnConsultaMiembroJuv = new javax.swing.JButton();
        btnEliminaMiembroJuv = new javax.swing.JButton();
        btnRefrescarJuv = new javax.swing.JButton();
        tipoDePrestamo = new javax.swing.ButtonGroup();
        dialogoLibros = new javax.swing.JDialog();
        jScrollPane5 = new javax.swing.JScrollPane();
        tablaLibros = new javax.swing.JTable();
        btnSeleccionarDiaLibros = new javax.swing.JButton();
        btnRefrescarLibros = new javax.swing.JButton();
        dialogoMiembrosGeneral = new javax.swing.JDialog();
        btnSeleccionarDiaMiembros = new javax.swing.JButton();
        jScrollPane6 = new javax.swing.JScrollPane();
        tablaMiembrosGeneral = new javax.swing.JTable();
        btnRefrescarMiembrosGeneral = new javax.swing.JButton();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        panMiembros = new javax.swing.JPanel();
        radioAdulto = new javax.swing.JRadioButton();
        radioJuvenil = new javax.swing.JRadioButton();
        panAdulto = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtCodMiembro = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtApellido = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtInicial = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtCalle = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtCP = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txtCiudad = new javax.swing.JTextField();
        txtEstado = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txtCelular = new javax.swing.JTextField();
        btnGuardar = new javax.swing.JButton();
        btnSeleccionar = new javax.swing.JButton();
        radioGuardado = new javax.swing.JRadioButton();
        radioConsulta = new javax.swing.JRadioButton();
        jLabel9 = new javax.swing.JLabel();
        txtnombre = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        txtFechaExp = new javax.swing.JTextField();
        btnLimpiar = new javax.swing.JButton();
        panJuvenil = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        txtMemJuv = new javax.swing.JTextField();
        txtApeJuv = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        txtNomJuv = new javax.swing.JTextField();
        txtIniJuv = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        txtFechNacJuv = new javax.swing.JTextField();
        btnGuardarJuv = new javax.swing.JButton();
        btnSeleccionarJuv = new javax.swing.JButton();
        jScrollPane7 = new javax.swing.JScrollPane();
        tablaMiembrosAlta = new javax.swing.JTable();
        btnRefrescarMiembrosG = new javax.swing.JButton();
        panPrestamos = new javax.swing.JPanel();
        panAltaPrestamo = new javax.swing.JPanel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        txtIsbnAlta = new javax.swing.JTextField();
        txtNumCopAlta = new javax.swing.JTextField();
        txtTitLibAlta = new javax.swing.JTextField();
        txtMiembroAlta = new javax.swing.JTextField();
        txtDiaEntrega = new javax.swing.JTextField();
        btnBuscaLibro = new javax.swing.JButton();
        btnBuscaMiembro = new javax.swing.JButton();
        btnPrestar = new javax.swing.JButton();
        panCatalogoPrestamos = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tablaPrestamos = new javax.swing.JTable();
        panBajaPrestamo = new javax.swing.JPanel();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        txtIsbnBaja = new javax.swing.JTextField();
        txtNumCopBaja = new javax.swing.JTextField();
        txtTitLibBaja = new javax.swing.JTextField();
        txtMiembroBaja = new javax.swing.JTextField();
        jScrollPane4 = new javax.swing.JScrollPane();
        txtComentario = new javax.swing.JTextArea();
        btnSeleccionaPrestamo = new javax.swing.JButton();
        btnDevolver = new javax.swing.JButton();
        radioAltaPrestamo = new javax.swing.JRadioButton();
        radioBajaPrestamo = new javax.swing.JRadioButton();
        btnRefrescarPrestamos = new javax.swing.JButton();
        panHistPrestamos = new javax.swing.JPanel();
        jScrollPane8 = new javax.swing.JScrollPane();
        tablaHistPrestamos = new javax.swing.JTable();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();

        dialogoMiembros.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        dialogoMiembros.setTitle("Miembros");
        dialogoMiembros.setBounds(new java.awt.Rectangle(0, 0, 700, 400));
        dialogoMiembros.setModal(true);
        dialogoMiembros.setResizable(false);

        tablaMiembros.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane2.setViewportView(tablaMiembros);

        btnConsultaMiembroAdul.setText("Consultar");
        btnConsultaMiembroAdul.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConsultaMiembroAdulActionPerformed(evt);
            }
        });

        btnEliminarMiembro.setText("Eliminar");
        btnEliminarMiembro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarMiembroActionPerformed(evt);
            }
        });

        btnRefrescar.setText("Refrescar");
        btnRefrescar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefrescarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout dialogoMiembrosLayout = new javax.swing.GroupLayout(dialogoMiembros.getContentPane());
        dialogoMiembros.getContentPane().setLayout(dialogoMiembrosLayout);
        dialogoMiembrosLayout.setHorizontalGroup(
            dialogoMiembrosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dialogoMiembrosLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(dialogoMiembrosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 680, Short.MAX_VALUE)
                    .addGroup(dialogoMiembrosLayout.createSequentialGroup()
                        .addComponent(btnConsultaMiembroAdul)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnEliminarMiembro)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnRefrescar)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        dialogoMiembrosLayout.setVerticalGroup(
            dialogoMiembrosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, dialogoMiembrosLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(dialogoMiembrosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnConsultaMiembroAdul)
                    .addComponent(btnEliminarMiembro)
                    .addComponent(btnRefrescar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 349, Short.MAX_VALUE)
                .addContainerGap())
        );

        dialogoMiembrosJuv.setBounds(new java.awt.Rectangle(0, 0, 500, 350));

        tablaMiembrosJuv.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tablaMiembrosJuv);

        btnConsultaMiembroJuv.setText("Consultar");
        btnConsultaMiembroJuv.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConsultaMiembroJuvActionPerformed(evt);
            }
        });

        btnEliminaMiembroJuv.setText("Eliminar");
        btnEliminaMiembroJuv.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminaMiembroJuvActionPerformed(evt);
            }
        });

        btnRefrescarJuv.setText("Refrescar");
        btnRefrescarJuv.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefrescarJuvActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout dialogoMiembrosJuvLayout = new javax.swing.GroupLayout(dialogoMiembrosJuv.getContentPane());
        dialogoMiembrosJuv.getContentPane().setLayout(dialogoMiembrosJuvLayout);
        dialogoMiembrosJuvLayout.setHorizontalGroup(
            dialogoMiembrosJuvLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dialogoMiembrosJuvLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(dialogoMiembrosJuvLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(dialogoMiembrosJuvLayout.createSequentialGroup()
                        .addComponent(btnConsultaMiembroJuv)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnEliminaMiembroJuv)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnRefrescarJuv)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 480, Short.MAX_VALUE))
                .addContainerGap())
        );
        dialogoMiembrosJuvLayout.setVerticalGroup(
            dialogoMiembrosJuvLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, dialogoMiembrosJuvLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(dialogoMiembrosJuvLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnConsultaMiembroJuv)
                    .addComponent(btnEliminaMiembroJuv)
                    .addComponent(btnRefrescarJuv))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 299, Short.MAX_VALUE)
                .addContainerGap())
        );

        dialogoLibros.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        dialogoLibros.setBounds(new java.awt.Rectangle(0, 0, 500, 400));

        tablaLibros.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane5.setViewportView(tablaLibros);

        btnSeleccionarDiaLibros.setText("Seleccionar");
        btnSeleccionarDiaLibros.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSeleccionarDiaLibrosActionPerformed(evt);
            }
        });

        btnRefrescarLibros.setText("Refrescar");
        btnRefrescarLibros.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefrescarLibrosActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout dialogoLibrosLayout = new javax.swing.GroupLayout(dialogoLibros.getContentPane());
        dialogoLibros.getContentPane().setLayout(dialogoLibrosLayout);
        dialogoLibrosLayout.setHorizontalGroup(
            dialogoLibrosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dialogoLibrosLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnSeleccionarDiaLibros)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnRefrescarLibros)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 500, Short.MAX_VALUE)
        );
        dialogoLibrosLayout.setVerticalGroup(
            dialogoLibrosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, dialogoLibrosLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(dialogoLibrosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSeleccionarDiaLibros)
                    .addComponent(btnRefrescarLibros))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 349, Short.MAX_VALUE)
                .addContainerGap())
        );

        dialogoMiembrosGeneral.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        dialogoMiembrosGeneral.setBounds(new java.awt.Rectangle(0, 0, 500, 400));

        btnSeleccionarDiaMiembros.setText("Seleccionar");
        btnSeleccionarDiaMiembros.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSeleccionarDiaMiembrosActionPerformed(evt);
            }
        });

        tablaMiembrosGeneral.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane6.setViewportView(tablaMiembrosGeneral);

        btnRefrescarMiembrosGeneral.setText("Refrescar");
        btnRefrescarMiembrosGeneral.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefrescarMiembrosGeneralActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout dialogoMiembrosGeneralLayout = new javax.swing.GroupLayout(dialogoMiembrosGeneral.getContentPane());
        dialogoMiembrosGeneral.getContentPane().setLayout(dialogoMiembrosGeneralLayout);
        dialogoMiembrosGeneralLayout.setHorizontalGroup(
            dialogoMiembrosGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dialogoMiembrosGeneralLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(dialogoMiembrosGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 480, Short.MAX_VALUE)
                    .addGroup(dialogoMiembrosGeneralLayout.createSequentialGroup()
                        .addComponent(btnSeleccionarDiaMiembros)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnRefrescarMiembrosGeneral)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        dialogoMiembrosGeneralLayout.setVerticalGroup(
            dialogoMiembrosGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dialogoMiembrosGeneralLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(dialogoMiembrosGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSeleccionarDiaMiembros)
                    .addComponent(btnRefrescarMiembrosGeneral))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 349, Short.MAX_VALUE)
                .addContainerGap())
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBounds(new java.awt.Rectangle(0, 0, 995, 500));
        setResizable(false);

        tipoDeMiembro.add(radioAdulto);
        radioAdulto.setSelected(true);
        radioAdulto.setText("Adulto");
        radioAdulto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioAdultoActionPerformed(evt);
            }
        });

        tipoDeMiembro.add(radioJuvenil);
        radioJuvenil.setText("Juvenil");
        radioJuvenil.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioJuvenilActionPerformed(evt);
            }
        });

        panAdulto.setBorder(javax.swing.BorderFactory.createTitledBorder("Miembro Adulto"));

        jLabel1.setText("Código Miembro");

        jLabel2.setText("Apellido");

        jLabel3.setText("Inicial");

        txtInicial.setEnabled(false);

        jLabel4.setText("Calle");

        jLabel5.setText("C.P.");

        jLabel6.setText("Ciudad");

        jLabel7.setText("Estado");

        jLabel8.setText("Celular");

        btnGuardar.setText("Guardar");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });

        btnSeleccionar.setText("Seleccionar");
        btnSeleccionar.setEnabled(false);
        btnSeleccionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSeleccionarActionPerformed(evt);
            }
        });

        tipoDeOperacion.add(radioGuardado);
        radioGuardado.setSelected(true);
        radioGuardado.setText("Guardado");
        radioGuardado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioGuardadoActionPerformed(evt);
            }
        });

        tipoDeOperacion.add(radioConsulta);
        radioConsulta.setText("Consulta ");
        radioConsulta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioConsultaActionPerformed(evt);
            }
        });

        jLabel9.setText("Nombre");

        jLabel15.setText("Fecha de Expiración");

        txtFechaExp.setEnabled(false);

        btnLimpiar.setText("Limpiar");
        btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panAdultoLayout = new javax.swing.GroupLayout(panAdulto);
        panAdulto.setLayout(panAdultoLayout);
        panAdultoLayout.setHorizontalGroup(
            panAdultoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panAdultoLayout.createSequentialGroup()
                .addComponent(btnGuardar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(radioGuardado)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(radioConsulta)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnLimpiar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnSeleccionar))
            .addGroup(panAdultoLayout.createSequentialGroup()
                .addGroup(panAdultoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panAdultoLayout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtCodMiembro, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtInicial, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel15)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtFechaExp))
                    .addGroup(panAdultoLayout.createSequentialGroup()
                        .addGroup(panAdultoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(panAdultoLayout.createSequentialGroup()
                                .addComponent(jLabel7)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtEstado, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panAdultoLayout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtCalle, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)
                        .addGroup(panAdultoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panAdultoLayout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtCP, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtCiudad))
                            .addGroup(panAdultoLayout.createSequentialGroup()
                                .addGap(9, 9, 9)
                                .addComponent(jLabel8)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtCelular, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(panAdultoLayout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtApellido)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtnombre, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(52, 52, 52)))
                .addContainerGap())
        );
        panAdultoLayout.setVerticalGroup(
            panAdultoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panAdultoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panAdultoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtCodMiembro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(txtInicial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel15)
                    .addComponent(txtFechaExp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panAdultoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtApellido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9)
                    .addComponent(txtnombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panAdultoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtCalle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(txtCP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6)
                    .addComponent(txtCiudad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panAdultoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(txtEstado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8)
                    .addComponent(txtCelular, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(panAdultoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnGuardar)
                    .addComponent(radioGuardado)
                    .addComponent(radioConsulta)
                    .addComponent(btnSeleccionar)
                    .addComponent(btnLimpiar))
                .addContainerGap(30, Short.MAX_VALUE))
        );

        panJuvenil.setBorder(javax.swing.BorderFactory.createTitledBorder("Miembro Juvenil"));

        jLabel10.setText("Código Miembro");

        jLabel11.setText("Apellido");

        jLabel12.setText("Nombre");

        jLabel13.setText("Inicial");

        txtIniJuv.setEnabled(false);

        jLabel14.setText("Fecha de Nacimiento");

        btnGuardarJuv.setText("Guardar");
        btnGuardarJuv.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarJuvActionPerformed(evt);
            }
        });

        btnSeleccionarJuv.setText("Seleccionar");
        btnSeleccionarJuv.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSeleccionarJuvActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panJuvenilLayout = new javax.swing.GroupLayout(panJuvenil);
        panJuvenil.setLayout(panJuvenilLayout);
        panJuvenilLayout.setHorizontalGroup(
            panJuvenilLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panJuvenilLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panJuvenilLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(panJuvenilLayout.createSequentialGroup()
                        .addComponent(jLabel10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtMemJuv, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel13)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtIniJuv, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panJuvenilLayout.createSequentialGroup()
                        .addComponent(jLabel12)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtNomJuv))
                    .addGroup(panJuvenilLayout.createSequentialGroup()
                        .addComponent(jLabel11)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtApeJuv, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panJuvenilLayout.createSequentialGroup()
                        .addComponent(jLabel14)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtFechNacJuv)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(panJuvenilLayout.createSequentialGroup()
                .addComponent(btnGuardarJuv)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnSeleccionarJuv))
        );
        panJuvenilLayout.setVerticalGroup(
            panJuvenilLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panJuvenilLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panJuvenilLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(panJuvenilLayout.createSequentialGroup()
                        .addGroup(panJuvenilLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel10)
                            .addComponent(txtMemJuv, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel13)
                            .addComponent(txtIniJuv, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel11))
                    .addComponent(txtApeJuv, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panJuvenilLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(txtNomJuv, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panJuvenilLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(txtFechNacJuv, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panJuvenilLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnGuardarJuv)
                    .addComponent(btnSeleccionarJuv))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        tablaMiembrosAlta.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tablaMiembrosAlta.setEnabled(false);
        jScrollPane7.setViewportView(tablaMiembrosAlta);

        btnRefrescarMiembrosG.setText("Refrescar");
        btnRefrescarMiembrosG.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefrescarMiembrosGActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panMiembrosLayout = new javax.swing.GroupLayout(panMiembros);
        panMiembros.setLayout(panMiembrosLayout);
        panMiembrosLayout.setHorizontalGroup(
            panMiembrosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panMiembrosLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panMiembrosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane7)
                    .addGroup(panMiembrosLayout.createSequentialGroup()
                        .addGroup(panMiembrosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panMiembrosLayout.createSequentialGroup()
                                .addComponent(radioAdulto)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(radioJuvenil)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnRefrescarMiembrosG))
                            .addComponent(panAdulto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(panJuvenil, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(352, 352, 352))
        );
        panMiembrosLayout.setVerticalGroup(
            panMiembrosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panMiembrosLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panMiembrosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(radioAdulto)
                    .addComponent(radioJuvenil)
                    .addComponent(btnRefrescarMiembrosG))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panMiembrosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(panJuvenil, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panAdulto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane7, javax.swing.GroupLayout.DEFAULT_SIZE, 207, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Miembros", panMiembros);

        panAltaPrestamo.setBorder(javax.swing.BorderFactory.createTitledBorder("Alta Préstamo"));

        jLabel16.setText("ISBN");

        jLabel17.setText("Número de copia");

        jLabel18.setText("Título del libro");

        jLabel19.setText("Miembro");

        jLabel20.setText("Día de entrega");

        txtIsbnAlta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtIsbnAltaActionPerformed(evt);
            }
        });

        btnBuscaLibro.setText("Buscar");
        btnBuscaLibro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscaLibroActionPerformed(evt);
            }
        });

        btnBuscaMiembro.setText("Buscar");
        btnBuscaMiembro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscaMiembroActionPerformed(evt);
            }
        });

        btnPrestar.setText("Prestar");
        btnPrestar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrestarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panAltaPrestamoLayout = new javax.swing.GroupLayout(panAltaPrestamo);
        panAltaPrestamo.setLayout(panAltaPrestamoLayout);
        panAltaPrestamoLayout.setHorizontalGroup(
            panAltaPrestamoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panAltaPrestamoLayout.createSequentialGroup()
                .addGroup(panAltaPrestamoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panAltaPrestamoLayout.createSequentialGroup()
                        .addGroup(panAltaPrestamoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(panAltaPrestamoLayout.createSequentialGroup()
                                .addComponent(jLabel19)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtMiembroAlta))
                            .addGroup(panAltaPrestamoLayout.createSequentialGroup()
                                .addComponent(jLabel18)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtTitLibAlta, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panAltaPrestamoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnBuscaMiembro)
                            .addComponent(btnBuscaLibro)))
                    .addGroup(panAltaPrestamoLayout.createSequentialGroup()
                        .addComponent(jLabel20)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtDiaEntrega, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnPrestar, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panAltaPrestamoLayout.createSequentialGroup()
                        .addComponent(jLabel16)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtIsbnAlta, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel17)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtNumCopAlta, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panAltaPrestamoLayout.setVerticalGroup(
            panAltaPrestamoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panAltaPrestamoLayout.createSequentialGroup()
                .addGroup(panAltaPrestamoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16)
                    .addComponent(txtIsbnAlta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel17)
                    .addComponent(txtNumCopAlta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panAltaPrestamoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel18)
                    .addComponent(txtTitLibAlta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBuscaLibro))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panAltaPrestamoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel19)
                    .addComponent(txtMiembroAlta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBuscaMiembro))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panAltaPrestamoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel20)
                    .addComponent(txtDiaEntrega, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnPrestar))
                .addContainerGap(21, Short.MAX_VALUE))
        );

        panCatalogoPrestamos.setBorder(javax.swing.BorderFactory.createTitledBorder("Préstamos"));

        tablaPrestamos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane3.setViewportView(tablaPrestamos);

        javax.swing.GroupLayout panCatalogoPrestamosLayout = new javax.swing.GroupLayout(panCatalogoPrestamos);
        panCatalogoPrestamos.setLayout(panCatalogoPrestamosLayout);
        panCatalogoPrestamosLayout.setHorizontalGroup(
            panCatalogoPrestamosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panCatalogoPrestamosLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 574, Short.MAX_VALUE)
                .addContainerGap())
        );
        panCatalogoPrestamosLayout.setVerticalGroup(
            panCatalogoPrestamosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panCatalogoPrestamosLayout.createSequentialGroup()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 114, Short.MAX_VALUE)
                .addContainerGap())
        );

        panBajaPrestamo.setBorder(javax.swing.BorderFactory.createTitledBorder("Baja Préstamo"));

        jLabel21.setText("ISBN");

        jLabel22.setText("Número de copia");

        jLabel23.setText("Título del libro");

        jLabel24.setText("Miembro");

        jLabel25.setText("Comentarios (100 letras)");

        txtComentario.setColumns(20);
        txtComentario.setRows(5);
        txtComentario.setToolTipText("Comentarios");
        jScrollPane4.setViewportView(txtComentario);

        btnSeleccionaPrestamo.setText("Seleccionar");
        btnSeleccionaPrestamo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSeleccionaPrestamoActionPerformed(evt);
            }
        });

        btnDevolver.setText("Devolver");
        btnDevolver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDevolverActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panBajaPrestamoLayout = new javax.swing.GroupLayout(panBajaPrestamo);
        panBajaPrestamo.setLayout(panBajaPrestamoLayout);
        panBajaPrestamoLayout.setHorizontalGroup(
            panBajaPrestamoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panBajaPrestamoLayout.createSequentialGroup()
                .addGroup(panBajaPrestamoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(panBajaPrestamoLayout.createSequentialGroup()
                        .addComponent(jLabel23)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtTitLibBaja, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel24)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtMiembroBaja))
                    .addGroup(panBajaPrestamoLayout.createSequentialGroup()
                        .addGroup(panBajaPrestamoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panBajaPrestamoLayout.createSequentialGroup()
                                .addComponent(jLabel21)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtIsbnBaja, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel22)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtNumCopBaja, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panBajaPrestamoLayout.createSequentialGroup()
                                .addComponent(jLabel25)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnSeleccionaPrestamo)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnDevolver)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        panBajaPrestamoLayout.setVerticalGroup(
            panBajaPrestamoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panBajaPrestamoLayout.createSequentialGroup()
                .addGroup(panBajaPrestamoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel21)
                    .addComponent(txtIsbnBaja, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel22)
                    .addComponent(txtNumCopBaja, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panBajaPrestamoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel23)
                    .addComponent(txtTitLibBaja, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel24)
                    .addComponent(txtMiembroBaja, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(14, 14, 14)
                .addGroup(panBajaPrestamoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel25)
                    .addComponent(btnSeleccionaPrestamo)
                    .addComponent(btnDevolver))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        tipoDePrestamo.add(radioAltaPrestamo);
        radioAltaPrestamo.setText("Alta");
        radioAltaPrestamo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioAltaPrestamoActionPerformed(evt);
            }
        });

        tipoDePrestamo.add(radioBajaPrestamo);
        radioBajaPrestamo.setText("Baja");
        radioBajaPrestamo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioBajaPrestamoActionPerformed(evt);
            }
        });

        btnRefrescarPrestamos.setText("Refrescar");
        btnRefrescarPrestamos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefrescarPrestamosActionPerformed(evt);
            }
        });

        panHistPrestamos.setBorder(javax.swing.BorderFactory.createTitledBorder("Historial de Préstamos"));

        tablaHistPrestamos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane8.setViewportView(tablaHistPrestamos);

        javax.swing.GroupLayout panHistPrestamosLayout = new javax.swing.GroupLayout(panHistPrestamos);
        panHistPrestamos.setLayout(panHistPrestamosLayout);
        panHistPrestamosLayout.setHorizontalGroup(
            panHistPrestamosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panHistPrestamosLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane8, javax.swing.GroupLayout.DEFAULT_SIZE, 574, Short.MAX_VALUE)
                .addContainerGap())
        );
        panHistPrestamosLayout.setVerticalGroup(
            panHistPrestamosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panHistPrestamosLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane8, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout panPrestamosLayout = new javax.swing.GroupLayout(panPrestamos);
        panPrestamos.setLayout(panPrestamosLayout);
        panPrestamosLayout.setHorizontalGroup(
            panPrestamosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panPrestamosLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panPrestamosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(panAltaPrestamo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(panPrestamosLayout.createSequentialGroup()
                        .addComponent(radioAltaPrestamo)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(radioBajaPrestamo)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnRefrescarPrestamos))
                    .addComponent(panBajaPrestamo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panPrestamosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panCatalogoPrestamos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(panHistPrestamos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panPrestamosLayout.setVerticalGroup(
            panPrestamosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panPrestamosLayout.createSequentialGroup()
                .addGroup(panPrestamosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panPrestamosLayout.createSequentialGroup()
                        .addGroup(panPrestamosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(radioAltaPrestamo)
                            .addComponent(radioBajaPrestamo)
                            .addComponent(btnRefrescarPrestamos))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(panAltaPrestamo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panPrestamosLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(panCatalogoPrestamos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panPrestamosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panPrestamosLayout.createSequentialGroup()
                        .addGap(0, 41, Short.MAX_VALUE)
                        .addComponent(panHistPrestamos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(panBajaPrestamo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        jTabbedPane1.addTab("Préstamo", panPrestamos);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 990, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 472, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("tab3", jPanel3);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 990, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 472, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("tab4", jPanel4);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void radioAdultoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioAdultoActionPerformed
        this.panAdulto.setVisible(true);
        this.panJuvenil.setVisible(false);
        
        this.radioConsulta.setEnabled(true);
        this.radioGuardado.setEnabled(true);
        this.radioGuardado.setSelected(true);
        
        this.txtCodMiembro.setEnabled(true);
        this.txtApellido.setEnabled(true);
        this.txtnombre.setEnabled(true);
        this.txtCalle.setEnabled(true);
        this.txtCP.setEnabled(true);
        this.txtCiudad.setEnabled(true);
        this.txtEstado.setEnabled(true);
        this.txtCelular.setEnabled(true);
        
        this.btnGuardar.setEnabled(true);
        this.btnSeleccionar.setEnabled(false);
       
    }//GEN-LAST:event_radioAdultoActionPerformed

    private void radioJuvenilActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioJuvenilActionPerformed
        this.panAdulto.setVisible(true);
        this.panJuvenil.setVisible(true);
        
        this.radioConsulta.setEnabled(false);
        this.radioConsulta.setSelected(true);
        this.radioGuardado.setEnabled(false);
        
        this.txtCodMiembro.setEnabled(false);
        this.txtApellido.setEnabled(false);
        this.txtnombre.setEnabled(false);
        this.txtCalle.setEnabled(false);
        this.txtCP.setEnabled(false);
        this.txtCiudad.setEnabled(false);
        this.txtEstado.setEnabled(false);
        this.txtCelular.setEnabled(false);
        
        this.btnGuardar.setEnabled(false);
        this.btnSeleccionar.setEnabled(true);
        
        
    }//GEN-LAST:event_radioJuvenilActionPerformed

    private void radioGuardadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioGuardadoActionPerformed
        this.txtCodMiembro.setEnabled(true);
        this.txtApellido.setEnabled(true);
        this.txtnombre.setEnabled(true);
        this.txtCalle.setEnabled(true);
        this.txtCP.setEnabled(true);
        this.txtCiudad.setEnabled(true);
        this.txtEstado.setEnabled(true);
        this.txtCelular.setEnabled(true);
        
        this.btnGuardar.setEnabled(true);
        this.btnSeleccionar.setEnabled(false);
    }//GEN-LAST:event_radioGuardadoActionPerformed

    private void radioConsultaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioConsultaActionPerformed
        this.txtCodMiembro.setEnabled(false);
        this.txtApellido.setEnabled(false);
        this.txtnombre.setEnabled(false);
        this.txtCalle.setEnabled(false);
        this.txtCP.setEnabled(false);
        this.txtCiudad.setEnabled(false);
        this.txtEstado.setEnabled(false);
        this.txtCelular.setEnabled(false);
        
        this.btnGuardar.setEnabled(false);
        this.btnSeleccionar.setEnabled(true);
    }//GEN-LAST:event_radioConsultaActionPerformed

    private void btnSeleccionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSeleccionarActionPerformed
        this.dialogoMiembros.setVisible(true);
        DefaultTableModel tm = Consulta.consultaMiembrosAdulto();
        this.tablaMiembros.setModel(tm);
        this.tablaMiembros.updateUI();
    }//GEN-LAST:event_btnSeleccionarActionPerformed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
       if(!bandConsulta){
           String inicial = this.txtApellido.getText().substring(0, 1);
            this.txtInicial.setText(inicial);
            Consulta.insertaMiembroAdulto(Integer.parseInt(this.txtCodMiembro.getText()),this.txtApellido.getText(),this.txtnombre.getText(),this.txtInicial.getText(),this.txtCalle.getText(),this.txtCP.getText(),this.txtCiudad.getText(),this.txtEstado.getText(),this.txtCelular.getText(),"01/01/2011");
            limpiarCasillasAdulto();
            this.txtCodMiembro.grabFocus();
            return;
       }
       //Código para realizar un UPDATE
        System.out.println("Función no disponible");
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void btnConsultaMiembroAdulActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConsultaMiembroAdulActionPerformed
        if(this.tablaMiembros.getSelectedRow() == -1){
           JOptionPane.showMessageDialog(null, "Favor de seleccionar una celda");
            return;
        }
        Object datos[] = new Object[this.tablaMiembros.getColumnCount()];
        for(int i = 0; i < datos.length;i++){
            datos[i] = this.tablaMiembros.getValueAt(this.tablaMiembros.getSelectedRow(), i);
        }
        int c = 0;
        this.txtCodMiembro.setText(datos[c++].toString());
        this.txtInicial.setText(datos[c++].toString());
        this.txtApellido.setText(datos[c++].toString());
        this.txtnombre.setText(datos[c++].toString());
        this.txtCalle.setText(datos[c++].toString());
        this.txtCP.setText(datos[c++].toString());
        this.txtCiudad.setText(datos[c++].toString());
        this.txtEstado.setText(datos[c++].toString());
        this.txtCelular.setText(datos[c++].toString());
        this.txtFechaExp.setText(datos[c].toString());
        this.dialogoMiembros.dispose();//Una vez consultado, tener la posibilidad de editarlo
        bandConsulta = true;//True es que hay algo en consulta
        if(bandConsulta){
            this.btnGuardar.setText("Editar");
        }
    }//GEN-LAST:event_btnConsultaMiembroAdulActionPerformed

    private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed
        limpiarCasillasAdulto();
        limpiarCasillasJoven();
        bandConsultaJuv = false;
        bandConsulta = false;
        this.btnGuardar.setText("Guardar");
        this.btnGuardarJuv.setText("Guardar");
    }//GEN-LAST:event_btnLimpiarActionPerformed

    private void btnGuardarJuvActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarJuvActionPerformed
        String inicial = this.txtApeJuv.getText().substring(0, 1);
        this.txtIniJuv.setText(inicial);
        Consulta.insertaMiembroJoven(Integer.parseInt(this.txtMemJuv.getText()),this.txtNomJuv.getText(),this.txtApeJuv.getText(),this.txtIniJuv.getText(),Integer.parseInt(this.txtCodMiembro.getText()),this.txtFechNacJuv.getText());
        limpiarCasillasJoven();
        this.txtMemJuv.grabFocus();
    }//GEN-LAST:event_btnGuardarJuvActionPerformed

    private void btnSeleccionarJuvActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSeleccionarJuvActionPerformed
        this.dialogoMiembrosJuv.setVisible(true);
        DefaultTableModel tm = Consulta.consultaMiembrosJoven();
        this.tablaMiembrosJuv.setModel(tm);
        this.tablaMiembrosJuv.updateUI();
    }//GEN-LAST:event_btnSeleccionarJuvActionPerformed

    private void btnRefrescarJuvActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefrescarJuvActionPerformed
        DefaultTableModel tm = Consulta.consultaMiembrosJoven();
        this.tablaMiembrosJuv.setModel(tm);
        this.tablaMiembrosJuv.updateUI();
    }//GEN-LAST:event_btnRefrescarJuvActionPerformed

    private void btnRefrescarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefrescarActionPerformed
        DefaultTableModel tm = Consulta.consultaMiembrosAdulto();
        this.tablaMiembros.setModel(tm);
        this.tablaMiembros.updateUI();
    }//GEN-LAST:event_btnRefrescarActionPerformed

    private void btnConsultaMiembroJuvActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConsultaMiembroJuvActionPerformed
        if(this.tablaMiembrosJuv.getSelectedRow() == -1){
           JOptionPane.showMessageDialog(null, "Favor de seleccionar una celda");
            return;
        }
        Object datosJ[] = new Object[this.tablaMiembrosJuv.getColumnCount()];
        Object datosA[] = Consulta.obtenerMiembroAdulto(Integer.parseInt(this.tablaMiembrosJuv.getValueAt(this.tablaMiembrosJuv.getSelectedRow(), 4).toString()));
        for(int i = 0; i < datosJ.length;i++){
            datosJ[i] = this.tablaMiembrosJuv.getValueAt(this.tablaMiembrosJuv.getSelectedRow(), i);
        }
        int c = 0;
        this.txtCodMiembro.setText(datosA[c++].toString());
        this.txtInicial.setText(datosA[c++].toString());
        this.txtApellido.setText(datosA[c++].toString());
        this.txtnombre.setText(datosA[c++].toString());
        this.txtCalle.setText(datosA[c++].toString());
        this.txtCP.setText(datosA[c++].toString());
        this.txtCiudad.setText(datosA[c++].toString());
        this.txtEstado.setText(datosA[c++].toString());
        this.txtCelular.setText(datosA[c++].toString());
        this.txtFechaExp.setText(datosA[c].toString());
        
        c = 0;
        this.txtMemJuv.setText(datosJ[c++].toString());
        this.txtApeJuv.setText(datosJ[c++].toString());
        this.txtNomJuv.setText(datosJ[c++].toString());
        this.txtIniJuv.setText(datosJ[c++].toString());
        c++;
        this.txtFechNacJuv.setText(datosJ[c].toString());
        
        this.dialogoMiembrosJuv.dispose();//Una vez consultado, tener la posibilidad de editarlo
        bandConsultaJuv = true;//True es que hay algo en consulta
        bandConsulta = true;
        if(bandConsultaJuv || bandConsulta){
            this.btnGuardarJuv.setText("Editar");
            this.btnGuardar.setText("Editar");
        }
    }//GEN-LAST:event_btnConsultaMiembroJuvActionPerformed

    private void btnEliminaMiembroJuvActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminaMiembroJuvActionPerformed
        if(this.tablaMiembrosJuv.getSelectedRow() == -1){
            JOptionPane.showMessageDialog(null, "Favor de seleccionar una celda");
            return;
        }
        Consulta.bajaMiembroJoven(Integer.parseInt(this.tablaMiembrosJuv.getValueAt(this.tablaMiembrosJuv.getSelectedRow(), 0).toString()));
        this.btnRefrescarJuv.doClick();
    }//GEN-LAST:event_btnEliminaMiembroJuvActionPerformed

    private void btnEliminarMiembroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarMiembroActionPerformed
       if(this.tablaMiembros.getSelectedRow() == -1){
            JOptionPane.showMessageDialog(null, "Favor de seleccionar una celda");
            return;
        }
        Consulta.bajaMiembroAdulto(Integer.parseInt(this.tablaMiembros.getValueAt(this.tablaMiembros.getSelectedRow(), 0).toString()));
        this.btnRefrescar.doClick();
    }//GEN-LAST:event_btnEliminarMiembroActionPerformed

    private void txtIsbnAltaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtIsbnAltaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtIsbnAltaActionPerformed

    private void btnPrestarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrestarActionPerformed
       int c = 1;
       Consulta.altaPrestamo(Integer.parseInt(libros[c++].toString()), Integer.parseInt(libros[c++].toString()), Integer.parseInt(libros[c].toString()), Integer.parseInt(miembrosGeneral[1].toString()), this.txtDiaEntrega.getText());
    }//GEN-LAST:event_btnPrestarActionPerformed

    private void radioAltaPrestamoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioAltaPrestamoActionPerformed
       this.panAltaPrestamo.setVisible(true);
       this.panBajaPrestamo.setVisible(false);
    }//GEN-LAST:event_radioAltaPrestamoActionPerformed

    private void radioBajaPrestamoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioBajaPrestamoActionPerformed
       this.panAltaPrestamo.setVisible(false);
       this.panBajaPrestamo.setVisible(true);
    }//GEN-LAST:event_radioBajaPrestamoActionPerformed

    private void btnBuscaLibroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscaLibroActionPerformed
        this.dialogoLibros.setVisible(true);
        DefaultTableModel tm = Consulta.consultaLibro();
        this.tablaLibros.setModel(tm);
        this.tablaLibros.updateUI();
    }//GEN-LAST:event_btnBuscaLibroActionPerformed

    private void btnRefrescarLibrosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefrescarLibrosActionPerformed
        DefaultTableModel tm = Consulta.consultaLibro();
        this.tablaLibros.setModel(tm);
        this.tablaLibros.updateUI();
    }//GEN-LAST:event_btnRefrescarLibrosActionPerformed

    private void btnSeleccionarDiaLibrosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSeleccionarDiaLibrosActionPerformed
        if(this.tablaLibros.getSelectedRow() == -1){
           JOptionPane.showMessageDialog(null, "Favor de seleccionar una celda");
            return;
        }
        libros = new Object[this.tablaLibros.getColumnCount()];//Variable global
        for(int i = 0; i < libros.length;i++){
            libros[i] = this.tablaLibros.getValueAt(this.tablaLibros.getSelectedRow(), i);
        }
        System.out.println("Se guardó el vector libros");
        int c = 0;
        this.txtTitLibAlta.setText(libros[c++].toString());
        c++;
        this.txtIsbnAlta.setText(libros[c++].toString());
        this.txtNumCopAlta.setText(libros[c].toString());
        this.dialogoLibros.dispose();
    }//GEN-LAST:event_btnSeleccionarDiaLibrosActionPerformed

    private void btnRefrescarMiembrosGeneralActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefrescarMiembrosGeneralActionPerformed
        DefaultTableModel tm = Consulta.consultaMiembroGeneral();
        this.tablaMiembrosGeneral.setModel(tm);
        this.tablaMiembrosGeneral.updateUI();
    }//GEN-LAST:event_btnRefrescarMiembrosGeneralActionPerformed

    private void btnBuscaMiembroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscaMiembroActionPerformed
        this.dialogoMiembrosGeneral.setVisible(true);
        DefaultTableModel tm = Consulta.consultaMiembroGeneral();
        this.tablaMiembrosGeneral.setModel(tm);
        this.tablaMiembrosGeneral.updateUI();
    }//GEN-LAST:event_btnBuscaMiembroActionPerformed

    private void btnSeleccionarDiaMiembrosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSeleccionarDiaMiembrosActionPerformed
        if(this.tablaMiembrosGeneral.getSelectedRow() == -1){
           JOptionPane.showMessageDialog(null, "Favor de seleccionar una celda");
            return;
        }
        miembrosGeneral = new Object[this.tablaMiembrosGeneral.getColumnCount()];//Variable global
        for(int i = 0; i < miembrosGeneral.length;i++){
            miembrosGeneral[i] = this.tablaMiembrosGeneral.getValueAt(this.tablaMiembrosGeneral.getSelectedRow(), i);
        }
        System.out.println("Se guardó el vector miembrosGeneral");
        //int c = 0;
        this.txtMiembroAlta.setText(MyRandom.PonBlancos(miembrosGeneral[0].toString(), 50));
        this.dialogoMiembrosGeneral.dispose();
    }//GEN-LAST:event_btnSeleccionarDiaMiembrosActionPerformed

    private void btnRefrescarPrestamosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefrescarPrestamosActionPerformed
        DefaultTableModel tm = Consulta.consultaPrestamos();
        this.tablaPrestamos.setModel(tm);
        this.tablaPrestamos.updateUI();
        tm = Consulta.consultaHistorialPrestamos();
        this.tablaHistPrestamos.setModel(tm);
        this.tablaHistPrestamos.updateUI();
    }//GEN-LAST:event_btnRefrescarPrestamosActionPerformed

    private void btnRefrescarMiembrosGActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefrescarMiembrosGActionPerformed
        DefaultTableModel tm = Consulta.consultaMiembroGeneral();
        this.tablaMiembrosAlta.setModel(tm);
        this.tablaMiembrosAlta.updateUI();
    }//GEN-LAST:event_btnRefrescarMiembrosGActionPerformed

    private void btnSeleccionaPrestamoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSeleccionaPrestamoActionPerformed
        if(this.tablaPrestamos.getSelectedRow() == -1){
           JOptionPane.showMessageDialog(null, "Favor de seleccionar una celda");
            return;
        }
        prestamos = new Object[this.tablaPrestamos.getColumnCount()];//Variable global
        for(int i = 0; i < prestamos.length;i++){
            prestamos[i] = this.tablaPrestamos.getValueAt(this.tablaPrestamos.getSelectedRow(), i);
        }
        System.out.println("Se guardó el vector prestamos");
        int c = 0;
        this.txtIsbnBaja.setText(prestamos[c++].toString());
        this.txtNumCopBaja.setText(prestamos[c++].toString());
        this.txtTitLibBaja.setText(Consulta.obtenerTituloLibroPorID(Integer.parseInt(prestamos[c++].toString())));
        this.txtMiembroBaja.setText(Consulta.obtenerNombreMiembroPorID(Integer.parseInt(prestamos[c].toString())));
    }//GEN-LAST:event_btnSeleccionaPrestamoActionPerformed

    private void btnDevolverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDevolverActionPerformed
        int c = 0;
        Consulta.bajaPrestamo(Integer.parseInt(prestamos[c++].toString()), Integer.parseInt(prestamos[c++].toString()), Integer.parseInt(prestamos[c++].toString()), Integer.parseInt(prestamos[c].toString()), this.txtComentario.getText());
    }//GEN-LAST:event_btnDevolverActionPerformed

    private void limpiarCasillasAdulto(){
        this.txtCodMiembro.setText("");
        this.txtInicial.setText("");
        this.txtApellido.setText("");
        this.txtnombre.setText("");
        this.txtCalle.setText("");
        this.txtCP.setText("");
        this.txtCiudad.setText("");
        this.txtEstado.setText("");
        this.txtCelular.setText("");
        this.txtFechaExp.setText("");
    }
    
    private void limpiarCasillasJoven(){
        this.txtMemJuv.setText("");
        this.txtIniJuv.setText("");
        this.txtApeJuv.setText("");
        this.txtNomJuv.setText("");
        this.txtFechNacJuv.setText("");
    }
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Biblioteca.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Biblioteca.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Biblioteca.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Biblioteca.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Biblioteca().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscaLibro;
    private javax.swing.JButton btnBuscaMiembro;
    private javax.swing.JButton btnConsultaMiembroAdul;
    private javax.swing.JButton btnConsultaMiembroJuv;
    private javax.swing.JButton btnDevolver;
    private javax.swing.JButton btnEliminaMiembroJuv;
    private javax.swing.JButton btnEliminarMiembro;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnGuardarJuv;
    private javax.swing.JButton btnLimpiar;
    private javax.swing.JButton btnPrestar;
    private javax.swing.JButton btnRefrescar;
    private javax.swing.JButton btnRefrescarJuv;
    private javax.swing.JButton btnRefrescarLibros;
    private javax.swing.JButton btnRefrescarMiembrosG;
    private javax.swing.JButton btnRefrescarMiembrosGeneral;
    private javax.swing.JButton btnRefrescarPrestamos;
    private javax.swing.JButton btnSeleccionaPrestamo;
    private javax.swing.JButton btnSeleccionar;
    private javax.swing.JButton btnSeleccionarDiaLibros;
    private javax.swing.JButton btnSeleccionarDiaMiembros;
    private javax.swing.JButton btnSeleccionarJuv;
    private javax.swing.JDialog dialogoLibros;
    public javax.swing.JDialog dialogoMiembros;
    private javax.swing.JDialog dialogoMiembrosGeneral;
    private javax.swing.JDialog dialogoMiembrosJuv;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    public javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JPanel panAdulto;
    private javax.swing.JPanel panAltaPrestamo;
    private javax.swing.JPanel panBajaPrestamo;
    private javax.swing.JPanel panCatalogoPrestamos;
    private javax.swing.JPanel panHistPrestamos;
    private javax.swing.JPanel panJuvenil;
    private javax.swing.JPanel panMiembros;
    private javax.swing.JPanel panPrestamos;
    private javax.swing.JRadioButton radioAdulto;
    private javax.swing.JRadioButton radioAltaPrestamo;
    private javax.swing.JRadioButton radioBajaPrestamo;
    private javax.swing.JRadioButton radioConsulta;
    private javax.swing.JRadioButton radioGuardado;
    private javax.swing.JRadioButton radioJuvenil;
    private javax.swing.JTable tablaHistPrestamos;
    private javax.swing.JTable tablaLibros;
    public javax.swing.JTable tablaMiembros;
    private javax.swing.JTable tablaMiembrosAlta;
    private javax.swing.JTable tablaMiembrosGeneral;
    private javax.swing.JTable tablaMiembrosJuv;
    private javax.swing.JTable tablaPrestamos;
    private javax.swing.ButtonGroup tipoDeMiembro;
    private javax.swing.ButtonGroup tipoDeOperacion;
    private javax.swing.ButtonGroup tipoDePrestamo;
    private javax.swing.JTextField txtApeJuv;
    private javax.swing.JTextField txtApellido;
    private javax.swing.JTextField txtCP;
    private javax.swing.JTextField txtCalle;
    private javax.swing.JTextField txtCelular;
    private javax.swing.JTextField txtCiudad;
    private javax.swing.JTextField txtCodMiembro;
    private javax.swing.JTextArea txtComentario;
    private javax.swing.JTextField txtDiaEntrega;
    private javax.swing.JTextField txtEstado;
    private javax.swing.JTextField txtFechNacJuv;
    private javax.swing.JTextField txtFechaExp;
    private javax.swing.JTextField txtIniJuv;
    private javax.swing.JTextField txtInicial;
    private javax.swing.JTextField txtIsbnAlta;
    private javax.swing.JTextField txtIsbnBaja;
    private javax.swing.JTextField txtMemJuv;
    private javax.swing.JTextField txtMiembroAlta;
    private javax.swing.JTextField txtMiembroBaja;
    private javax.swing.JTextField txtNomJuv;
    private javax.swing.JTextField txtNumCopAlta;
    private javax.swing.JTextField txtNumCopBaja;
    private javax.swing.JTextField txtTitLibAlta;
    private javax.swing.JTextField txtTitLibBaja;
    private javax.swing.JTextField txtnombre;
    // End of variables declaration//GEN-END:variables
}
