/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author guill
 */
public class Consulta {
    
    private static Conexion c;
    
    public static DefaultTableModel consultaMiembrosJoven(){
        c = new Conexion();
        DefaultTableModel tm = new DefaultTableModel();
        String sql;
        try{
            sql = "SELECT  m.member_no,m.lastname,m.name,m.middleinitial,j.adult_member_no,CONVERT(date,j.birthdate) AS Nacimiento FROM member m INNER JOIN juvenile j ON j.member_no = m.member_no GROUP BY m.member_no,m.lastname,m.name,m.middleinitial,j.adult_member_no,CONVERT(date,j.birthdate)";
            c.conectar();
            c.stmt = c.con.createStatement();
            c.rs = c.stmt.executeQuery(sql);
            tm = muestraData(c.rs);
        }catch(Exception e){
            
        }
        finally{
            c.desconectar();
        }
        return tm;
    }
    
    public static DefaultTableModel consultaMiembrosAdulto(){
        c = new Conexion();
        DefaultTableModel tm = new DefaultTableModel();
        String sql;
        try{
            sql = "SELECT M.member_no, m.middleinitial, m.lastname,m.name,a.street,a.zipcode,a.city,a.state,a.phone_no,CONVERT(date,a.expr_date) FROM member m INNER JOIN adult a ON a.member_no = m.member_no GROUP BY  M.member_no, m.middleinitial, m.lastname,m.name,a.street,a.zipcode,a.city,a.state,a.phone_no,CONVERT(date,a.expr_date)"; 
            c.conectar();
            c.stmt = c.con.createStatement();
            c.rs = c.stmt.executeQuery(sql);
            tm = muestraData(c.rs);
        }catch(Exception e){
            System.out.println("Error en consulta: " + e.getMessage());
        }
        finally{
             c.desconectar();
        }
        return tm;
    }
    
    public static DefaultTableModel consultaPrestamos(){
        c = new Conexion();
        DefaultTableModel tm = new DefaultTableModel();
        String sql;
         try{
            sql = "SELECT * FROM LOAN";
            c.conectar();
            c.stmt = c.con.createStatement();
            c.rs = c.stmt.executeQuery(sql);
            tm = muestraData(c.rs);
        }catch(Exception e){
            System.out.println("Error en consulta: " + e.getMessage());
        }
        finally{
             c.desconectar();
        }
        return tm;
    }
    
    public static DefaultTableModel consultaHistorialPrestamos(){
        c = new Conexion();
        DefaultTableModel tm = new DefaultTableModel();
        String sql;
         try{
            sql = "SELECT * FROM LOANHIST";
            c.conectar();
            c.stmt = c.con.createStatement();
            c.rs = c.stmt.executeQuery(sql);
            tm = muestraData(c.rs);
        }catch(Exception e){
            System.out.println("Error en consulta: " + e.getMessage());
        }
        finally{
             c.desconectar();
        }
        return tm;
    }
    
    public static void insertaMiembroAdulto(int id, String apellido,String nombre ,String inicial, String calle, String cp, String ciudad, String estado, String cel,String date) {
        c = new Conexion();
        try{
            c.conectar();
            c.pstmt = c.con.prepareStatement("{call Alta_Miembro_Adulto("+ id +",'"+ apellido +"','"+ nombre +"','"+ inicial +"','"+ calle +"','"+ cp +"','"+ ciudad +"','"+ estado +"','"+ cel +"','"+ date +"')}");
           c.pstmt.execute();
            JOptionPane.showMessageDialog(null, "Se insertó miembro adulto correctamente");
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Inserta Miembro Adulto: " + e.getMessage());
        }
        finally{
            c.desconectar();
        }
    }
    
    private static DefaultTableModel muestraData(ResultSet r) throws Exception{
        DefaultTableModel dtm = new DefaultTableModel();
        Object datos[];
        ResultSetMetaData rmeta= r.getMetaData();
        int numColumnas = rmeta.getColumnCount();       
        datos = new Object[numColumnas];
        for(int i=0; i < numColumnas; i++) {
            dtm.addColumn(rmeta.getColumnName(i+1));  
        }
        while(r.next()){
            for(int j = 0; j < numColumnas; j++){
                datos[j] = r.getString(j+1);
            }
            dtm.addRow(datos);
        }
        return dtm;
    }
    
    public static void insertaMiembroJoven(int idM,String ap,String nom,String ini,int idA,String feNa){//Crear insercion de alta joven!!
        c = new Conexion();
        try{
            c.conectar();
            c.pstmt = c.con.prepareStatement("{call Alta_Miembro_Joven("+ idM +",'"+ nom +"','"+ ap +"','"+ ini +"',"+ idA +",'"+ feNa +"')}");
            c.pstmt.execute();
             JOptionPane.showMessageDialog(null, "Se insertó miembro joven correctamente");
        }catch(Exception e){
             JOptionPane.showMessageDialog(null, "Inserta Miembro Joven: " + e.getMessage());
        }
        finally{
            c.desconectar();
        }
    }
    
    public static void altaPrestamo(int tn, int isbn, int cn, int mn, String dd){
        c = new Conexion();
        try{
            c.conectar();
           c.pstmt = c.con.prepareStatement("{call Alta_reservacion("+ isbn +","+ cn +","+ tn +","+ mn +",'"+ dd +"')}");
           c.pstmt.execute();
           JOptionPane.showMessageDialog(null, "Préstamo aplicado correctamente");
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Alta préstamo: " + e.getMessage());
        }
        finally{
            c.desconectar();
        }
    }
    
    public static Object[] obtenerMiembroAdulto(int idM){
        Object datos[] = new Object[0];
        c = new Conexion();
        String sql = "SELECT M.member_no, m.middleinitial, m.lastname,m.name,a.street,a.zipcode,a.city,a.state,a.phone_no,CONVERT(date,a.expr_date) AS Expiracion FROM member m INNER JOIN adult a ON a.member_no = m.member_no WHERE a.member_no = "+ idM +" GROUP BY  M.member_no, m.middleinitial, m.lastname,m.name,a.street,a.zipcode,a.city,a.state,a.phone_no,CONVERT(date,a.expr_date)";
        try{
            c.conectar();
            c.stmt = c.con.createStatement();
            c.rs = c.stmt.executeQuery(sql);
            ResultSetMetaData rmeta= c.rs.getMetaData();
            int numColumnas = rmeta.getColumnCount();
            datos = new Object[numColumnas];
            while(c.rs.next()){
                for(int i = 0; i < numColumnas; i++){
                    datos[i] = c.rs.getString(i+1);
                }
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Error Obtener Miembro: "+e.getMessage());
        }
        return datos;
    }
    
    public static String obtenerNombreMiembroPorID(int idM){
        String dato = "";
        c = new Conexion();
        String sql = "SELECT CONCAT(m.name, ' ' ,m.lastname) FROM member m WHERE m.member_no = "+idM+"";
        try{
            c.conectar();
            c.stmt = c.con.createStatement();
            c.rs = c.stmt.executeQuery(sql);
            while(c.rs.next()){
                dato = c.rs.getString(1);
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Error Obtener Miembro: "+e.getMessage());
        }
        return dato;
    }
    
    public static String obtenerTituloLibroPorID(int idT){
        String dato = "";
        c = new Conexion();
        String sql = "select t.title from title t where t.title_no = "+idT+"";
        try{
            c.conectar();
            c.stmt = c.con.createStatement();
            c.rs = c.stmt.executeQuery(sql);
            while(c.rs.next()){
                dato = c.rs.getString(1);
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Error Obtener Libro: "+e.getMessage());
        }
        return dato;
    }
    
    public static void bajaMiembroJoven(int idM){
        c = new Conexion();
        try{
            c.conectar();
            c.pstmt = c.con.prepareStatement("{call Baja_Miembro_Joven("+ idM +")}");
            c.pstmt.execute();
            JOptionPane.showMessageDialog(null, "Se eliminó miembro joven correctamente");
        }catch(Exception e){
             JOptionPane.showMessageDialog(null, "Baja Miembro Joven: " + e.getMessage());
        }
        finally{
            c.desconectar();
        }
    }
    
    public static void bajaMiembroAdulto(int idM){
        c = new Conexion();
        try{
            c.conectar();
            c.pstmt = c.con.prepareStatement("{call Baja_Miembro_Adulto("+ idM +")}");
            c.pstmt.execute();
            JOptionPane.showMessageDialog(null, "Se eliminó miembro adulto correctamente");
        }catch(Exception e){
             JOptionPane.showMessageDialog(null, "Baja Miembro adulto: " + e.getMessage());
        }
        finally{
            c.desconectar();
        }
    }
    
    public static void bajaPrestamo(int isbn, int cn, int tn, int mn, String cmn){
        c = new Conexion();
        try{
            c.conectar();
            c.pstmt = c.con.prepareStatement("{call Baja_reservacion("+ isbn +","+ cn +","+ tn +","+ mn +",'"+ cmn +"')}");
            c.pstmt.execute();
            JOptionPane.showMessageDialog(null, "Libro regresado con éxito");
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Error al regresar el libro: " + e.getMessage());
        }
        finally{
            c.desconectar();
        }
    }
    
    public static DefaultTableModel consultaLibro(){//Modeficar
        c = new Conexion();
        DefaultTableModel tm = new DefaultTableModel();
        String sql;
        try{
            sql = "select t.title,t.title_no,c.isbn,c.copy_no from title t join item i on i.title_no = t.title_no join copy c on c.isbn = i.isbn where on_loan = 'no'"; 
            c.conectar();
            c.stmt = c.con.createStatement();
            c.rs = c.stmt.executeQuery(sql);
            tm = muestraData(c.rs);
        }catch(Exception e){
            System.out.println("Error en consulta: " + e.getMessage());
        }
        finally{
             c.desconectar();
        }
        return tm;
    }
    
    public static DefaultTableModel consultaMiembroGeneral(){
        c = new Conexion();
        DefaultTableModel tm = new DefaultTableModel();
        String sql;
        try{
            sql = "SELECT CONCAT(m.name, ' ' ,m.lastname) AS Nombre , m.member_no FROM member m"; //Error en la consulta, el nombvre esta raro
            c.conectar();
            c.stmt = c.con.createStatement();
            c.rs = c.stmt.executeQuery(sql);
            tm = muestraData(c.rs);
        }catch(Exception e){
            System.out.println("Error en consulta: " + e.getMessage());
        }
        finally{
             c.desconectar();
        }
        return tm;
    }
}
