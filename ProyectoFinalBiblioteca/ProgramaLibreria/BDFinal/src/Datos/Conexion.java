/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Conexion {
    Connection con = null;
    Statement stmt = null;
    ResultSet rs = null;
    PreparedStatement pstmt = null;
    
    public void conectar(){
         String connectionUrl = "jdbc:sqlserver://localhost:1433;" +
            "databaseName=Libreria3;user=sa; password=1234;";      
        
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            con = DriverManager.getConnection(connectionUrl);
            System.out.println("Conexion exitosa");
            
        } 
        catch (Exception ex){
            System.out.println("Error: " + ex.getMessage());
        }
    }
    
    public void desconectar(){
        if (rs != null) try { rs.close(); } catch(Exception e) {}
        if (stmt != null) try { stmt.close(); } catch(Exception e) {}
        if (con != null) try { con.close(); } catch(Exception e) {}
        if (pstmt != null) try {pstmt.close(); } catch(Exception e) {}
        System.out.println("Desconectado");
    }
    
    
}
