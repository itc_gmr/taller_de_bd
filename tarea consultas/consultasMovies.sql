﻿use MoviesDB
--1.  Encontrar los títulos de todas las películas dirigidas por Steven Spielberg.
select director from movie where director = 'Steven Spielberg'
--2.  Mostrar todos los años que tienen una película que recibió un rating de 4 o 5, y ordénalo en orden ascendente.
select ratingDate from rating where stars = 4 or stars = 5
order by stars asc
--3.  Encuentra los nombres de todos los revisores que evaluaron (rated) la película Gone with the wind.
select r.name from reviewer r
join rating ra on ra.rID = r.rID
join movie m on m.mID = ra.mID
where m.title = 'Gone in the wild'
--4.  Para cualquier película donde el revisor es el mismo que el director de la película, regresa el nombre del revisor, el titulo de la película y el numero de estrellas.
select name,title,stars
from rating rat 
join reviewer rev on rev.rID = rat.rID
join movie m on m.mID = rat.mID
where m.director like rev.name
--5.  Escribe una consulta que regrese la información sobre el rating en un formato mas legible: nombre del revisor, titulo de la película, estrellas, y ratingdate. También, ordena los datos, primero por nombre del revisor, luego por titulo de la película, y al final por numero de estrellas.


