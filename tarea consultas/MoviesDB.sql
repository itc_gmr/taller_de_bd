use MoviesDB

/*
Base de datos MoviesDB
Alexis Hern�ndez
*/

--Crear Base de datos si no existe
CREATE DATABASE MoviesDB

--Eliminar tablas
DROP TABLE Movie
DROP TABLE Reviewer
DROP TABLE Rating

/*
	There is a movie with ID number mID, a title, a release year, and a director.
*/

CREATE TABLE Movie  (
    mID INT NOT NULL PRIMARY KEY,
    title CHAR(30) NOT NULL,
    year INT NOT NULL	CONSTRAINT YEAR_NOT_VALID check( year > 1900	AND	year < 2014 ),
    director CHAR(30) NULL
);

-- Llenar tabla Movie-
INSERT Movie VALUES	( 
	101, 'Gone with the Wind', 1939, 'Victor Fleming'
);
INSERT Movie VALUES	( 
	102, 'Star Wars', 1977, 'George Lucas'
);
INSERT Movie VALUES	( 
	103,'The sound of music',1965,'Robert Wise'
);
INSERT Movie VALUES	( 
	104,'ET',1982,'Steven Spielberg'
);
INSERT Movie VALUES	( 
	105,'Titanic',1997,'James Cameron'
);
INSERT Movie VALUES	( 
	106,'Snow white',1937,NULL
);
INSERT Movie VALUES	( 
	107,'Avatar',2009,'James Cameron'
);
INSERT Movie VALUES	( 
	108,'Raiders of the Lost Ark',1981,'Steven Spielberg'
);


/*
	The reviewer with ID number rID has a certain name.
*/
CREATE TABLE Reviewer   (
    rID INT NOT NULL PRIMARY KEY,
    name CHAR(30) NOT NULL
);

-- llenar tabla reviewer
INSERT Reviewer VALUES	( 
	201,'Sarah Martinez'
);
INSERT Reviewer VALUES	( 
	202,'Daniel Lewis'
);
INSERT Reviewer VALUES	( 
	203,'Britanny Harrys'
);
INSERT Reviewer VALUES	( 
	204,'Mike Anderson'
);
INSERT Reviewer VALUES	( 
	205,'Chris Jackson'
);
INSERT Reviewer VALUES	( 
	206,'Elizabeth Thomas'
);
INSERT Reviewer VALUES	( 
	207,'James Cameron'
);
INSERT Reviewer VALUES	( 
	208,'Ashley White'
);


/*
	The reviewer rID gave the movie mID a number of stars rating (1-5) on a certain ratingDate.
*/
CREATE TABLE Rating (
    rID INT NOT NULL references reviewer(rID),
    mID INT NOT NULL references movie(mID),
    stars INT NOT NULL	CONSTRAINT STARS_MUST_BE_BETWEEN_1_AND_5 CHECK( stars > 0	AND	stars < 6 ),
    ratingDate DATETIME NULL
);

-- llenando la tabla rating
Set DateFormat MDY --indicates the general format is Month Day Year

INSERT Rating VALUES	(
	201,101,2,'2011-01-22'
);
INSERT Rating VALUES	(
	201,101,4,'2011-01-27'
);
INSERT Rating VALUES	(
	202,106,4,NULL
);
INSERT Rating VALUES	(
	203,103,2,'2011-01-20'
);
INSERT Rating VALUES	(
	203,108,4,'2011-01-12'
);
INSERT Rating VALUES	(
	203,108,2,'2011-01-30'
);
INSERT Rating VALUES	(
	204,101,3,'2011-01-09'
);
INSERT Rating VALUES	(
	205,103,3,'2011-01-27'
);
INSERT Rating VALUES	(
	205,104,2,'2011-01-22'
);
INSERT Rating VALUES	(
	205,108,4,NULL
);
INSERT Rating VALUES	(
	206,107,3,'2011-01-15'
);
INSERT Rating VALUES	(
	206,106,5,'2011-01-19'
);
INSERT Rating VALUES	(
	207,107,5,'2011-01-20'
);
INSERT Rating VALUES	(
	208,104,3,'2011-01-02'
);

--SELECT B�SICO--
use MoviesDB

--1.  Encontrar los t�tulos de todas las pel�culas dirigidas por Steven Spielberg.
SELECT title 
FROM movie 
WHERE director = 'Steven Spielberg'

--2.  Mostrar todos los a�os que tienen una pel�cula que recibi� un rating de 4 o 5, y ord�nalo en orden ascendente.
SELECT ratingDate 
FROM rating 
WHERE stars = 4 or stars = 5
ORDER BY stars ASC

--3.  Encuentra los nombres de todos los revisores que evaluaron (rated) la pel�cula Gone with the wind.
SELECT r.name 
FROM reviewer r
JOIN rating ra ON ra.rID = r.rID
JOIN movie m ON m.mID = ra.mID
WHERE m.title = 'Gone with the wind'

--4.  Para cualquier pel�cula donde el revisor es el mismo que el director de la pel�cula, regresa el nombre del revisor, el titulo de la pel�cula y el numero de estrellas.
SELECT name,title,stars
FROM rating rat 
JOIN reviewer rev ON rev.rID = rat.rID
JOIN movie m ON m.mID = rat.mID
WHERE m.director LIKE rev.name

--5.  Escribe una consulta que regrese la informaci�n sobre el rating en un formato mas legible: nombre del revisor, titulo de la pel�cula, estrellas, y ratingdate. Tambi�n, ordena los datos, primero por nombre del revisor, luego por titulo de la pel�cula, y al final por numero de estrellas.
SELECT rev.name,m.title,rat.stars,rat.ratingDate
FROM Rating rat
JOIN Reviewer rev ON rev.rID = rat.rID
JOIN Movie m ON m.mID = rat.mID
ORDER BY rev.name,m.title,rat.stars

--6.  Regresa el nombre de todas las pel�culas y el nombre de los revisores en orden del alfab�tico (ordenando primero por el nombre del revisor y luego por los t�tulos que el revis�)
SELECT m.title,rev.name
FROM Movie m
JOIN Rating rat ON rat.mID = m.mID 
JOIN Reviewer rev ON rev.rID = rat.rID
ORDER BY rev.name,m.title

--7.  Encuentra los t�tulos de todas las pel�culas que no han sido revisadas por chris Jackson 
SELECT m.title,rev.name
FROM Movie m
JOIN Rating rat ON rat.mID = m.mID
JOIN Reviewer rev ON rev.rID = rat.rID
WHERE rev.name NOT IN(SELECT name FROM Reviewer WHERE name = 'Chris Jackson')

--Subqueries in WHERE, FROM, SELECT--

--10.- Qu� pel�culas tienen la evaluaci�n mas pobre en nuestra base de datos? : Mostrar los t�tulos de todas las pel�culas que no tienen rating.
SELECT m.title
FROM Movie m
JOIN Rating rat ON rat.mID = m.mID
WHERE rat.stars NOT IN(SELECT stars FROM Rating)

--11.  - Evaluaciones mas severas: Mostrar el nombre de los revisores, el t�tulos de la pel�cula y el n�mero de estrellas para todas las pel�culas que han recibido el rating menor.
SELECT top (SELECT top 1 COUNT(mID) FROM Rating GROUP BY stars ORDER BY stars ASC) 
rev.name, m.title,rat.stars
FROM Reviewer rev
JOIN Rating rat ON rat.rID = rev.rID
JOIN Movie m ON m.mID = rat.mID
ORDER BY rat.stars ASC

--12.-Conservar la mejor, ignora el resto: por cada pel�cula que tenga al menos un rating, encuentre el n�mero de estrellas mayor que la pel�cula ha recibido. Muestra el titulo de la pel�cula y el n�mero de estrellas. Ordenadas por t�tulo de la pel�cula. (Esta consulta la puedes resolver sin agregaci�n).
SELECT m.title, MAX(rat.stars) AS 'M�ximo rating'
FROM Reviewer rev
JOIN Rating rat ON rat.rID = rev.rID
JOIN Movie m ON m.mID = rat.mID
GROUP BY m.title
ORDER BY m.title
--�?

--Aggregation--

--13.  Calculemos algunas estad�sticas: mostrar los t�tulos de las pel�culas y el promedio ordenado del rating del mas alto al mas bajo
SELECT m.title, AVG(rat.stars) AS 'Promedio'
FROM Reviewer rev
JOIN Rating rat ON rat.rID = rev.rID
JOIN Movie m ON m.mID = rat.mID
GROUP BY m.title
ORDER BY AVG(rat.stars) DESC

--14.  Cual es las pel�cula mas controversial? :Por cada pel�cula, regresa el titulo y el �rating sreap�, esto es: la diferencia entre el rating mayor y el rating menor que se dio a la pel�cula. Ordena el resultado de mayor a menor y luego por el titulo de la pel�cula.
SELECT m.title, COUNT(rat.stars)
FROM Reviewer rev
JOIN Rating rat ON rat.rID = rev.rID
JOIN Movie m ON m.mID = rat.mID
GROUP BY m.title

SELECT m.title
FROM Reviewer rev
JOIN Rating rat ON rat.rID = rev.rID
JOIN Movie m ON m.mID = rat.mID
GROUP BY  m.title ------------------PENDIENTE

--15.  Usuarios poderosos: Encuentra el nombre de todos los revisores que han hecho tres o mas rating (puntos extras si lo haces sin Having o sin count).
SELECT rev.name
FROM Reviewer rev
JOIN Rating rat ON rat.rID = rev.rID
GROUP BY rev.name
HAVING COUNT(name) >= 3 --HAY OTRA OPCION

