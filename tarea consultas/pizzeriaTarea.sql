﻿CREATE DATABASE Pizzeria
go

create table pizza
(
cve_pizza char(5) not null constraint pizza_pk primary key,
pizza char(20) not null,
num_ingredientes int not null
)

insert into pizza values('P1','Champiñones',5)
insert into pizza values('P2','Quesos',7)
insert into pizza values('P3','Sinaloense',5)
insert into pizza values('P4','Dieta',4)
insert into pizza values('P5','Mozarella',8)
insert into pizza values('P6','Tres Marias',8)
insert into pizza values('P7','Italiana',8)
insert into pizza values('P8','Hawaiana',7)
go

select * from pizza
go

create table pizzeria
(
cve_pizzeria char(3) not null constraint pizzeria_pk primary key,
nombre char(20) not null,
localizacion char(20) not null
)

insert into pizzeria values('A','Fabula','Culiacán')
insert into pizzeria values('B','Pizzeta','Mazatlán')
insert into pizzeria values('C','Dominos','Culiacán')
insert into pizzeria values('D','Dominator','Culiacán')
insert into pizzeria values('E','Italianis','Los Mochis')
insert into pizzeria values('F','Dominos','Mazatlán')
insert into pizzeria values('G','Metro','Mazatlán')
insert into pizzeria values('H','Pizzerola','Mazatlán')
go

select * from pizzeria
go

create table sirven
(
cve_pizzeria char(3) not null constraint sirven_fk foreign key references pizzeria(cve_pizzeria),
cve_pizza char(5) not null constraint sirven_fk2 foreign key references pizza(cve_pizza),
constraint sirven_pk primary key(cve_pizzeria,cve_pizza)
)

insert into sirven values('A','P1')
insert into sirven values('A','P3')
insert into sirven values('B','P5')
insert into sirven values('B','P3')
insert into sirven values('C','P2')
insert into sirven values('D','P6')
insert into sirven values('D','P5')
insert into sirven values('A','P2')
insert into sirven values('B','P6')
insert into sirven values('D','P7')
go

SELECT * FROM sirven
go

create table cliente
(
cve_cte char(5) not null constraint cliente_pk primary key,
nombre char(20) not null,
edad int not null
)

insert into cliente values('C1','Juan',18)
insert into cliente values('C2','Rosa',17)
insert into cliente values('C3','Pedro',29)
insert into cliente values('C4','Tere',16)
insert into cliente values('C5','Alma',35)
go

select * from cliente
go

create table comen
(
cve_cte char(5) not null constraint comen_fk foreign key references cliente(cve_cte),
cve_pizza char(5) not null constraint comen_fk2 foreign key references pizza(cve_pizza),
constraint comen_pk primary key(cve_cte,cve_pizza)
)

insert into comen values('C1','P2')
insert into comen values('C1','P4')
insert into comen values('C4','P2')
insert into comen values('C5','P2')
insert into comen values('C5','P6')
insert into comen values('C5','P3')
insert into comen values('C5','P4')
insert into comen values('C2','P5')
insert into comen values('C2','P3')
insert into comen values('C3','P2')
insert into comen values('C3','P7')
insert into comen values('C5','P5')
go

SELECT * FROM comen
go

--1.- Nombre de las pizzerías de la ciudad de Culiacán.
select nombre 
from pizzeria 
where localizacion = 'Culiacán'
go
--2.- Nombre de las pizzerías que sirven mas de dos pizzas.
select p.nombre 
from pizzeria p
join sirven s on s.cve_pizzeria = p.cve_pizzeria
group by (p.nombre)
having count(p.nombre) > 2
go
--3.- Nombre de los clientes que comen mas de 3 pizzas.
select c.nombre 
from cliente c
join comen com on com.cve_cte = c.cve_cte
group by(c.nombre)
having COUNT(c.nombre) > 3
go
--4.- Nombre de las pizzas y el nombre de las pizzerías donde se sirven.
select piz.pizza, p.nombre 
from pizza piz
join sirven sir on sir.cve_pizza = piz.cve_pizza
join pizzeria p on p.cve_pizzeria = sir.cve_pizzeria
group by p.nombre, piz.pizza
go
--5.- Nombre de las pizzerías que sirven pizzas con mas de 3 ingredientes.
select p.nombre
from pizzeria p
join sirven s on s.cve_pizzeria = p.cve_pizzeria
join pizza piz on piz.cve_pizza = s.cve_pizza
group by p.nombre
having count(piz.num_ingredientes) >= 3 
go
--6.- Nombre de los clientes mayores que 18 años que comen pizzas de mozzarella.
select c.nombre 
from cliente c
join comen com on com.cve_cte = c.cve_cte
join pizza piz on piz.cve_pizza = com.cve_pizza
where c.edad > 18
group by c.nombre
go
--7.- Nombre de las pizza que no come ningún cliente.
select pizza
from pizza
where pizza not in (select piz.pizza
from pizza piz
join comen com on com.cve_pizza = piz.cve_pizza
join cliente c on c.cve_cte = com.cve_cte
group by piz.pizza)
go
--8.- mostrar una relación de pizzerías con el número de pizzas que sirve.
select p.nombre as 'Nombre Pizzería',count(s.cve_pizza) as 'Número de pizzas'
from pizzeria p
join sirven s on s.cve_pizzeria = p.cve_pizzeria
group by p.nombre
go
--9 .- Mostrar una relación de clientes con el número de pizzas que consume.
select c.nombre as 'Nombre Cliente', count(com.cve_pizza) as 'Pizzas consumidas'
from cliente c
join comen com on com.cve_cte = c.cve_cte
group by c.nombre
go
-- 10.- Nombre del cliente que mas pizzas consume (autoestudio: estudiar el TOP)
select top 1 c.nombre
from cliente c
join comen com on com.cve_cte = c.cve_cte
group by c.nombre
order by count(com.cve_pizza) desc
go
--11.- Nombre del cliente que menos pizzas consume (autoestudio: estudiar el TOP)
select top 1 c.nombre
from cliente c
join comen com on com.cve_cte = c.cve_cte
group by c.nombre
order by count(com.cve_pizza) asc
go
--12.- Mostrar la edad promedio de las personas que consumen pizzas de dieta.
select avg(c.edad) as 'Edad promedio'
from cliente c
join comen com on com.cve_cte = c.cve_cte
join pizza piz on piz.cve_pizza = com.cve_pizza
where piz.pizza like 'Dieta'
group by piz.pizza
go
--13.- Mostrar la pizzería que menos pizzas sirve.
select top 1 p.nombre
from pizzeria p
join sirven s on s.cve_pizzeria = p.cve_pizzeria
group by p.nombre
order by COUNT(s.cve_pizza) asc
go
--14.- Mostrar la pizzería que mas pizzas sirve.
select top 1 p.nombre
from pizzeria p
join sirven s on s.cve_pizzeria = p.cve_pizzeria
group by p.nombre
order by COUNT(s.cve_pizza) desc
go
--15.- Mostrar el nombre de la pizzería que no se sirve en ninguna pizzería. ¿¿¿??? Mostrar el nombre de la pizzería que no se sirve ninguna pizza
select nombre
from pizzeria
where nombre not in(select p.nombre
from pizzeria p
join sirven s on s.cve_pizzeria = p.cve_pizzeria
group by p.nombre)
go
--16.- Mostrar la ciudad que tiene mas de dos pizzerías.¿?
select p.localizacion
from pizzeria p
group by p.localizacion
having COUNT(p.localizacion) > 2
go
--17.- Mostrar el nombre de las pizzerías que se encuentran en la ciudad que tiene mas de dos pizzerías.¿?
select p.nombre, p.localizacion
from pizzeria p
where p.localizacion 
in(
select top 1 localizacion
from pizzeria
group by localizacion
order by COUNT(localizacion) desc
)

--Borrado de datos:
--1.- Borra los clientes que no consumen pizzas.
select * from cliente
go

delete from cliente 
where cliente.cve_cte not in(
select cl.nombre 
from cliente cl
join comen co on co.cve_cte = cl.cve_cte)
--2.- Elimina las pizzas que no se sirven en ninguna pizzería.
--3.- Eliminas los clientes menores de 19 años, que sucede:____________________________________________________________
--4.- Elimina las pizas de la ciudad de culiacan:________________________________________________




